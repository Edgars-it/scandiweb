$(function () {
    $('.product').change(function () {
        var option = $(this).find('option:selected');
        var value = '.' + option.val();
        $('.show-form').hide();
        $('.show-form input, .show-form select').removeAttr('required');
        $(value).show(300);
        $(value + ' input, ' + value + ' select').attr('required', true);
    });
    $('.product').change();
});
