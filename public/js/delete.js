$(function () {
    $('.choose').change(function () {
        var option = $(this).find('option:selected');
        var value = '.' + option.val();
        $('.delete, .check').hide();
        $(value).show(200);
    });
    $('.choose').change();
});
