<?php
// FOR DEBUGGING
require 'application/lib/Dev.php';

// Defines file locations
define('ROOT_PATH', dirname(__FILE__));
define('APPLICATION_PATH', ROOT_PATH . '/application');
define('PUBLIC_PATH', ROOT_PATH . '/public');
define('CONFIG_PATH', APPLICATION_PATH . '/config');

// Use of namespaces
use application\core\Router;

// Autoloads classes
spl_autoload_register(function ($class) {
    $path = str_replace('\\', '/', $class . '.php');
    if (file_exists($path)) {
        require $path;
    }
});

// Starts SESSION
session_start();

//Initializes Router and runs it
$router = new Router;
$router->run() . '<br>';
