<?php

namespace application\core;

use application\core\View;

use application\products\Dvd;
use application\products\Book;
use application\products\Furniture;

/**
 * Router class
 */
class Router
{

    protected $routes = [];
    protected $params = [];

    public function __construct()
    {
        $array = require CONFIG_PATH . '/routes.php';
        foreach ($array as $key => $value) {
            $this->add($key, $value);
        }
    }

    public function add($route, $params)
    {
        //Adds regexp to route
        $route = '#^' . $route . '$#';
        //Adds to routes array key value pair of passed $route and $params
        $this->routes[$route] = $params;
    }

    public function match()
    {
        //Gets current url and trims it
        $url = trim($_SERVER['REQUEST_URI'], '/');
        foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $matches)) {
                //Passes to params array matched route
                $this->params = $params;
                return true;
            }
        }
        return false;
    }

    public function run()
    {
        //Looks for match in route
        if ($this->match()) {
            //Creates path with found controller and adds Controller to found controller: ex = MainController
            $path = 'application\controllers\\' . ucfirst($this->params['controller']) . 'Controller';
            //Checks if class exists
            if (class_exists($path)) {
                //Adds to action word Action: ex = indexAction
                $action = $this->params['action'] . 'Action';
                //Checks if method exists
                if (method_exists($path, $action)) {
                    //Creates controller class
                    $controller = new $path($this->params);
                    //and runs action found in url
                    $controller->$action();
                } else {
                    View::error_code(404);
                }
            } else {
                View::error_code(404);
            }
        } else {
            View::error_code(404);
        }
    }
}
