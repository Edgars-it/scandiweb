<?php

namespace application\core;

use application\core\View;

abstract class Controller
{
    public $route;
    public $view;

    public function __construct($route)
    {
        $this->route = $route;
        $this->view = new View($route);
        $this->model = $this->load_model($route['controller']);
    }

    //Loads model and passes it to Controller
    public function load_model($name)
    {
        $class = 'application\models\\' . ucfirst($name);
        if (class_exists($class)) {
            return new $class;
        }
    }
}
