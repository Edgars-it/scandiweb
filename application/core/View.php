<?php

namespace application\core;

class View
{
    //Default layout for pages
    public $layout = 'default';
    public $path;
    public $route;

    public function __construct($route)
    {
        $this->route = $route;
        $this->path = $route['controller'] . '/' . $route['action'];
    }

    //Renders page and inserts view into $content variable
    public function render($title, $vars = [])
    {
        extract($vars);
        ob_start();
        require 'application/views/' . $this->path . '.php';
        //
        $content = ob_get_clean();
        require 'application/views/layouts/' . $this->layout . '.php';
    }

    //Gets .php file and displays it`s content ( example 404.php file can contain html page for 404 code)
    public static function error_code($code)
    {
        http_response_code($code);
        require 'application/views/errors/' . $code . '.php';
        exit;
    }
}
