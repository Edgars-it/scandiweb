<div class="container">
    <?php foreach ($products as $val) : ?>
        <h4><?php echo $val['name']; ?></h4>
        <p><?php echo $val['price']; ?>$</p>
        <hr>
    <?php endforeach; ?>

    <br><br>
    <button class="main-btn"><a href="/products/list">Show detailed list of products</a></button>
    <button class="main-btn"><a href="/product/add">Add product</a></button>
</div>