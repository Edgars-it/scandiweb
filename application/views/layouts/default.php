<?php
if (!isset($page_title)) {
    $page_title = 'Product';
}
if (!isset($h_title)) {
    $h_title = 'menu';
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ScandiWeb <?php echo $page_title; ?></title>
    <link rel="stylesheet" href="/public/css/main.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body>
    <div class="page">
        <header class="nav">
            <h1>Product <?php echo $h_title; ?></h1>
        </header>

        <?php echo $content ?>

        <footer>
            <h6>&copy; <?php echo date('Y') . ' by ED for SandiWeb'; ?></h6>
        </footer>
    </div>
    <script src="/public/js/delete.js"></script>
    <script src="/public/js/hideForm.js"></script>
</body>

</html>