<?php
$page_title = '| Product Add';
$h_title = 'add';
?>

<form class="form" action="/product/add" method="post">
    <input class="button" name="submit" type="submit" value="Save" />
    <table>
        <tr>
            <td>
                <label for="sku">SKU: </label>
            </td>
            <td>
                <input type="text" id="sku" name="sku" required />
            </td>
        </tr>
        <tr>
            <td>
                <label for="name">Name: </label>
            </td>
            <td>
                <input type="text" id="name" name="name" required />
            </td>
        </tr>
        <tr>
            <td>
                <label for="price">Price: </label>
            </td>
            <td>
                <input type="text" id="price" name="price" required />
            </td>
        </tr>
        <tr>
            <td>
                <label for="product">Select type of product: </label>
            </td>
            <td>
                <select id="product" class="product" name="product">
                    <option value="none" default>----- Select product from list below -----</option>
                    <option value="dvd">DVD disc</option>
                    <option value="book">Book</option>
                    <option value="furniture">Furniture</option>
                </select>
            </td>
        </tr>
    </table>

    <fieldset class="show-form dvd">
        <legend>DVD size: </legend>
        <div>
            <label for="dvd">Size: </label>
            <input class="dvd" id="dvd" type="number" step="0.001" min="0" name="size" placeholder="Enter DVD size" />
            <p>Please provide DVD disc size in MB</p>
        </div>
    </fieldset>

    <fieldset class="show-form book">
        <legend>Book weight: </legend>
        <div>
            <label for="book">Weight: </label>
            <input class="book" id="book" type="number" step="0.001" min="0" name="weight" placeholder="Enter weight" />
            <p>Please provide books weight in Kg</p>
        </div>
    </fieldset>

    <fieldset class="show-form furniture">
        <legend>Furniture dimensions: </legend>
        <table>
            <tr>
                <td>
                    <label for="height">Height: </label>
                </td>
                <td>
                    <input class="furniture" id="height" type="number" min="0" name="height" placeholder="Height of furniture" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="width">Width: </label>
                </td>
                <td>
                    <input class="furniture" id="width" type="number" min="0" name="width" placeholder="Width of furniture" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="length">Length: </label>
                </td>
                <td>
                    <input class="furniture" id="length" type="number" min="0" name="length" placeholder="Length of furniture" />
                </td>
            </tr>
        </table>
        <p>Please provide dimensions for furniture in HxWxL format</p>
    </fieldset>
</form>