<?php
$page_title = '| Product List';
$h_title = 'list';

?>
<form class="form-list" action="/products/list" method="post">
    <select class="choose" name="" id="delete">
        <option value="none">None</option>
        <option value="delete">Mass delete</option>
    </select>
    <input class="button delete" type="submit" value="Delete" onclick="return confirm('Are you sure?')" />

    <?php foreach ($products as $value) : ?>
        <div class="window">
            <input type="checkbox" name="checked_id[]" class="check delete" value="<?php echo $value['id']; ?>">
            <p class="skus"> <?php echo $value['sku']; ?></p>
            <p> <?php echo $value['name']; ?></p>
            <p> <?php echo $value['price']; ?> $</p>
            <p> <?php echo $value['description']; ?></p>
        </div>
    <?php endforeach; ?>
</form>