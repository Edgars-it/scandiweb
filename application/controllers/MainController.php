<?php

namespace application\controllers;

use application\core\Controller;

class MainController extends Controller
{

    public function indexAction()
    {
        //Lists products
        $result = $this->model->show_products();
        $vars = [
            'products' => $result,
        ];
        $this->view->render('ScandiWeb | Main page', $vars);
    }
}
