<?php

namespace application\controllers;

use application\core\Controller;

class ProductController extends Controller
{
    public function listAction()
    {
        //Deletes products
        if (!empty($_POST)) {
            $this->model->delete();
        }
        //Lists products
        $result = $this->model->get_products();
        $vars = [
            'products' => $result,
        ];
        $this->view->render('ScandiWeb | Product list', $vars);
    }

    public function addAction()
    {
        //Saves product
        if (!empty($_POST)) {
            $this->model->save($_POST);
        }
        $this->view->render('ScandiWeb | Product add');
    }
}
