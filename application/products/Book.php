<?php

use application\models\Product;

class Book extends Product
{

    public function save($post)
    {
        $date = date("Y-m-d H:m:s");
        $description = $this->set_description();
        $params = [
            'id' => '0',
            'sku' => $post['sku'],
            'name' => $post['name'],
            'price' => $post['price'],
            'description' => $description,
            'created' => $date,
        ];
        $this->db->query('INSERT INTO products VALUES (:id, :sku, :name, :price, :description, :created)', $params);
    }

    private function set_description()
    {
        return $_POST['weight'] . ' Kg';
    }
}
