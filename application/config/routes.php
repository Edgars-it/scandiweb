<?php

return [

    '' => [
        'controller' => 'main',
        'action' => 'index',
    ],

    'products/list' => [
        'controller' => 'product',
        'action' => 'list',
    ],

    'product/add' => [
        'controller' => 'product',
        'action' => 'add',
    ],

];
