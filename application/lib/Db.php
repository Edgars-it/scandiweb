<?php

namespace application\lib;

use PDO;
use PDOException;

/**
 * Class for connecting to database. Database configuration parameters are stored in application/config/dbcredentials.php
 */
class Db
{

    protected $db;

    public function __construct()
    {
        try {
            $db_config = require 'application/config/dbcredentials.php';
            $this->db = new PDO('mysql:host=' . $db_config['host'] . ';dbname=' . $db_config['dbname'], $db_config['user'], $db_config['password']);
            $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (PDOException $ex) {
            echo 'Connection failed: ' . $ex->getMessage();
            exit;
        }
    }

    public function query($sql, $parameters = [])
    {
        $statement = $this->db->prepare($sql);
        if (!empty($parameters)) {
            foreach ($parameters as $key => $value) {
                $statement->bindValue(':' . $key, $value);
            }
        }
        $statement->execute();
        return $statement;
    }

    public function row($sql, $parameters = [])
    {
        $result = $this->query($sql, $parameters);
        return $result->fetchAll();
    }

    public function delete()
    {
        $checked = $_POST['checked_id'];
        $array_id = implode(',', $checked);
        $result = $this->db->prepare("DELETE FROM products WHERE id in ($array_id)");
        $result->execute();
    }
}
