<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

/**
 * Function for debugging.
 * @param string
 */
function debug($string)
{
    echo '<pre>';
    var_dump($string);
    echo '</pre>';
    exit;
}
