<?php

namespace application\models;

use application\core\Model;


class Product extends Model
{

    public function get_products()
    {
        $result = $this->db->row('select id, sku, name, price, description from products');
        return $result;
    }

    public function save($post)
    {
        if (isset($post['submit'])) {
            if (isset($post['product'])) {
                $class = ucfirst($post['product']);
                require_once 'application/products/' . $class . '.php';
                $newClass = new $class;
                $newClass->save($post);
            }
        }
    }

    public function delete()
    {
        $this->db->delete();
    }
}
