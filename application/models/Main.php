<?php

namespace application\models;

use application\core\Model;

class Main extends Model
{
    public function show_products()
    {
        $result = $this->db->row('select sku, name, price, description from products');
        return $result;
    }
}
